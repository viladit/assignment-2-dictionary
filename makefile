OBJDIR=objects
OUTFILE=main.out
OBJECT_FILES=$(patsubst %.asm, $(OBJDIR)/%.o, $(wildcard *.asm))
INC_FILES=$(wildcard *.inc)

.PHONY: all test clean

all: $(OUTFILE)

$(OUTFILE): $(OBJECT_FILES)
	ld -o $@ $^

$(OBJDIR)/%.o: %.asm $(INC_FILES) objdir_create
	nasm -g -f elf64 -o $@ $<

.PHONY: objdir_create
objdir_create:
	mkdir -p $(OBJDIR)

test: $(OUTFILE)
	bash test.sh

clean:
	rm -rf $(OBJDIR) $(OUTFILE)
