test_expect()
{
  if [[ "$2" == "$3" ]]; then
    echo "Test $1 passed"
  else
    echo "Test $1 failed"
    echo "Expected '$3', but got '$2'"
  fi
}

output=`echo "depression" | ./main.out 2> /dev/null`
expected="depression is everywhere"

test_expect "#1" "$output" "$expected"

output=`echo "sadness" | ./main.out 2> /dev/null`
expected="today i have been feeling sadness whole day"

test_expect "#2" "$output" "$expected"

output=`echo "hunger" | ./main.out 2> /dev/null`
expected="i am starving"

test_expect "#3" "$output" "$expected"

output=`echo "what is it" | ./main.out 2>&1 > /dev/null`
expected="Wasn't found"

test_expect "#4" "$output" "$expected"

output=`printf "%255s" | tr ' ' 'A' | ./main.out 2> /dev/null`
expected="Buffer overflowed"

test_expect "#5" "$output" "$expected"


output=`printf "%256s" | tr ' ' 'B' | ./main.out 2> /dev/null`
expected="Buffer overflowed"

test_expect "#6" "$output" "$expected"
