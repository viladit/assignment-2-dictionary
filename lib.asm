section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line
global print_error
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    test rdi, rdi      ; Проверка на нулевой указатель
    jz .end            ; Если rdi равен 0, завершаем выполнение функции
.loop:
    mov r10b, [rdi + rax]
    inc rax
    test r10b, r10b
    jnz .loop
.end:
    dec rax
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi;
    call string_length
    pop rsi;
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 2
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
	mov r8, rsp
	push 0
	mov r9, 10 
	.loop:
		xor rdx, rdx
		div r9
		add rdx, '0'
		dec rsp
		mov byte[rsp], dl
		test rax, rax   
		jnz .loop 
	mov rdi, rsp
	push r8
	call print_string
	pop rsp
	
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi         		  ; Сравнение числа со значением 0
    jge print_uint            ; if >= 0 goto print_unit
    push rdi                
    mov rdi, 45          	  ;"-"
    call print_char           
    pop rdi                   
    neg rdi                   ; Инверсия числа
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax       ; 0 -> rax

loop:
    mov al, byte [rdi]
    cmp al, byte [rsi]
    jne not_equal      ; If lines're not equal goto not_equal
    cmp al, 0          
    je equal

    inc rdi            
    inc rsi            
    jmp loop

equal:
    mov rax, 1         
    ret                

not_equal:
    mov rax, 0         
    ret                

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    dec rsp
    mov rsi, rsp
    syscall
    test rax, rax
    jz .end_file
    mov al, [rsp]
    .end_file:
        inc rsp
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    .loop:
        call read_char          ; вызов функции read_char
        test rax, rax
        jz .yes                 ; если символ равен 0, то переход на .yes
        cmp r14, r13
        jnl .great              ; если r14>=r13, то переход на .great
        cmp rax, 0xA        
        jz .space               ; проверка на пробельный символ
        cmp rax, 0x9
        jz .space               ; проверка на пробельный символ
        cmp rax, 0x20
        jz .space               ; проверка на пробельный символ
        mov [r12 + r14], al     ; запись символа в память
        inc r14
        cmp r14, r13            ; проверка на превышение размера буфера
        jge .yes
        jmp .loop
    .yes:
         mov byte[r14+r12],0   ; запись нуль-терминированного элемента
         mov rax, r12          
         mov rdx, r14
         jmp .end
    .space:
         test r14,r14          ; проверка на то что r14==0
         je .loop
         jmp .yes
    .great:
        xor rax, rax           ; 0->rax
    .end:
        pop r14
        pop r13
        pop r12
        ret
 
 read_line:
  push r12
  push r13
  push r14
  mov r12, rdi
  mov r13, rsi
  mov r14, 1
  test r13, r13
  jz .overflow
  .read_loop:
    call read_char
    mov byte[r12], al
    test al, al
    jz .return
    cmp al, `\n`
    jz .return
    cmp r14, r13
    jae .overflow
    inc r14
    inc r12
    jmp .read_loop
  .return:
    mov byte[r12], 0
    mov rdx, r14
    dec rdx
    mov rax, r12
    sub rax, rdx
    pop r14
    pop r13
    pop r12
    ret
  .overflow:
    pop r14
    pop r13
    pop r12
    xor rax, rax
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r10, r10 ;длина
    mov rcx, 10
    .loop:
        mov r9, rax
        xor rax, rax
        mov al, byte[rdi]
        test rax, rax
        jz .good
        mov rsi, rax ;rsi - считаная цифра
        mov rax, r9 ;число из пршлых итерация
        sub sil, '0'
        cmp sil, 0
        jb .bad
        cmp sil, 10
        ja .good
        mul rcx
        add rax, rsi
        inc r10
        inc rdi
        jmp .loop
    .good:
        mov rax, r9
        mov rdx, r10
        ret
    .bad:
        xor rdx, rdx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor     rax, rax            ; 0 -> rax
    mov     al, byte [rdi]      ; load 1st char of line
    cmp     al, '-'             ; check '-'
    je      .neg           		; if '-' goto neg
    cmp     al, '+'             ; check '+'
    je      .pos     			; if '+' goto pos
	.pos:
		jmp    parse_uint           
	.neg:
		inc     rdi                 
		call    parse_uint          
		neg     rax                 
		inc     rdx                 
		ret                         

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    mov rax, rdx
    mov rdi, rdi
    mov rsi, rsi
    rep movsb
    test byte [rsi - 1], 0 
    jnz .success

.fail:
    xor rax, rax
.success:
    ret   
