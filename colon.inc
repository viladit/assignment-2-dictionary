%define begin_ref 0x0
%define dict_key_ptr_offset 8
%define dict_value_ptr_offset 16

; the macro takes as input a key string and the name of a label, by which to store the corresponding value
; first comes the address of the next pair (current x1)
; then comes the address where the key string is saved
; then the address where the value is stored

%macro colon 2
    %ifstr %1
        %ifid %2
            %%key:
                db %1, 0
            %%element:
                dq begin_ref
                dq %%key
                dq %2
            %2:
                %define begin_ref %%element
        %else
            %error "Second argument should be a label"
        %endif
    %else
        %error "First argument should be a string"
    %endif
%endmacro

; The macro takes an arbitrary number of arguments as input (assuming that these are strings).
; Checks whether the input is a null-terminated string. If not, appends 0 to the end.
; Warns an error when reading a non-null-terminated string.
%macro db 1-*
    %rep %0
        db %1
        %rotate 1
    %endrep
    %rotate -1
    %ifstr %1
        %ifnidn %1, "\0"
            db 0
        %endif
    %else
        %if %1 <> 0
            db 0
        %endif
    %endif
%endmacro
