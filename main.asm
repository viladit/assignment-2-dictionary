%include "lib.inc"
%include "colon.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_SIZE 255

section .rodata

overflow_msg: db "buffer is overflowed", 0
not_found_msg: db "Key wasn't found", 0

section .bss

read_buffer: resb BUFFER_SIZE

section .text

global _start
_start:
  mov rdi, read_buffer
  mov rsi, BUFFER_SIZE
  call read_line
  test rax, rax
  jz .overflow
  mov rdi, rax
  mov rsi, dictionary
  call find_word
  test rax, rax
  jz .not_found
  mov rdi, rax
  call get_entry_value
  mov rdi, rax
  call print_string
  call print_newline
  xor rdi, rdi
  jmp exit
  .not_found:
    mov rdi, not_found_msg
    jmp exit_with_error
  .overflow:
    mov rdi, overflow_msg
    jmp exit_with_error    

exit_with_error:
  call print_error
  call print_newline
  mov rdi, 1
  jmp exit